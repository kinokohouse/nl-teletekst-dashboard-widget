# NL Teletext Dashboard Widget for macOS #
Dashboard widget giving access to a number (currently, exactly one) of Dutch-language teletext services. Source available under an MIT license. Made in DashCode. Ready to use binary available from downloads section. Tested on 10.6.8 and above, should work on systems as early as 10.4.3. Actually, couldn't make network connection with 10.6.8 during last debugging, might also not work on 10.7 or other systems, but works neatly on 10.14. Caveat Emptor.

Dashboard-widget welke toegang geeft tot een aantal (op dit moment precies, zegge en schrijve, één) Nederlandstalige teletekstdiensten. Broncode beschikbaar onder een MIT-licensie. Gemaakt in DashCode. Kant-en-klare widget te verkrijgen in de downloads-sectie. Getest op 10.6.8 en daarboven, zou moeten werken vanaf 10.4.3. Nee, wacht even, werkte niet op 10.6.8 tijdens laatste debugging, misschein ook niet op 10.7 of iets daarboven, maar werkt goed op 10.14. Geen garanties!

Building
--------
Editing and deploying can be done from either version of DashCode, or you can edit the files within the widget bundle with your favourite text/image editors. 

Bewerken en exporteren kan in beide versies van DashCode, of door het bewerken van de bestanden in de widget-bundel met je favoriete tekst- en beeldbewerkingssoftware.

Version info
------------

Current version/Huidige versie: 3.00 220703 Stand Alone Editie\*

\* Versions also get a nickname with which usually the cancellation of yet another teletext service is lamented. Since now only NOS Teletext remains, we'll call this version 'Stand Alone Edition'. This teletext widget is the only program I know of that gains in version number as it loses features! ;)

\* Versies krijgen altijd een editienaam waarmee doorgaans de teloorgang van WEER een teletekstdienst wordt beklaagd. Deze keer moeten we afscheid nemen van alles behalve NOS, daarom 'Stand Alone Editie'. Teletekst: de enige widget die een hoger versienummer krijgt als er features verdwijnen! ;)
