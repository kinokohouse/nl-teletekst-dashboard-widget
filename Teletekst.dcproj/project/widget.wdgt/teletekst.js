// Versie 3.00 220703 Stand Alone Editie
//
// Opgedragen aan die ene gebruiker die zo nu en dan deze widget nog gebruikt


attachListeners();

var objList = ["inputfield", "currentpage", "pinmessage"];
var sloganCarousel = ["De oudjes doen het nog best","Tech van toen, nieuws van nu","Dit was het nieuws","Waar is pagina 316 gebleven?","En dan dit, of toch maar...?","Op de digitale toer","Analoog is zo gek nog niet","Altijd actueel en beschikbaar","Snel! Pagina 101!","Meer heb je niet nodig","'s Ochtends als allereerste","Nu met 40% meer nieuw(s)!", "Vroeger was alles beter"];

var animationRunning = false;
var timerIsActive = false;
var timerWasActive = false;
var autoReloadTimer = null;
var sloganCounter = 0;
var busyCounter = 0;
var currentlyFront = 0;
var inputBlocked = false;
var waitingForKeyUp = false;
var exceptFlag = false;
var by = {timer: null, counter: 0, flag: false}
var scan = {test: -1,p: 0, flag: false, t:50, timer: null, counter: 0}
var generalDelay = 300;
var animSteps = 1;
var animReady = 30;
var oneShot = null;
var autoReloadTimerJiffies = 0;
var transLostFocus = true;
var viewTimer = null;
var pageScan = 0;
var waitInterval = 1500;
var basePage = "100";
var homePage = "100";
var nextPage = "100";
var prevPage = "100";
var lastKnownGoodPage = "000";
var subPage = "01";
var maxSub = 1;
var curSub = 1;
var stealthSubUp = "00";
var stealthSubDown = "00";
var upSubAllowed = false;
var downSubAllowed = false;
var nosList = null;
var memArray = [];
var memPointer = 0;
var maxMemSize = 10;
var skipMemAdd = false;
var inputPage = "";
var allowNonExistentPages = false;
var scanOnCursorKeys = true;
var direction = true;
var backOn = false;
var trans = new Transition(Transition.DISSOLVE_TYPE, 0.5);

var T  = function () {
	return (new Date).getTime();
}

var T2 = function () {
    var d      = new Date;
    var theDay = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear();
    var hours  = d.getHours();
    if (hours < 9) hours = "0" + hours;
    var mins   = d.getMinutes();
    if (mins < 9) mins = "0" + mins;
    var secs   = d.getSeconds();
    if (secs < 9) secs = "0" + secs;
    var theTime = hours + ":" + mins + ":" + secs
    return theDay + "%20" + theTime;
}

var debug = false;
var memCheck = false;

function load() {
    dashcode.setupParts();
    initWidget();
    document.getElementById("pageinput").innerHTML = basePage;
    document.getElementById("pintext").innerHTML = "Startpagina: " + homePage;
    loadPage(basePage);
    teleText.style.opacity = 1;
}

function remove() {
    detachListeners();
    stopAllTimers();
    deleteTransientPrefs();
}

function hide() {
    detachListeners();
    timerWasActive = timerIsActive;
    stopReloadTimer();
}

function show() {
    attachListeners();
    if (timerWasActive) {
        reloadCurrentPage()
    }
    else {
        justReloadCurrentPage();
    }
}

function sync() {

}

function showBack(event) {
    backOn = true;
    detachListeners();
    stopReloadTimer();
    document.getElementById("slogan").innerHTML = sloganCarousel[sloganCounter];
    document.getElementById("thanxtext").innerHTML = "Font &#39;Bedstead&#39; door <a onclick=\"javascript:openWebPage('https://bjh21.me.uk/');\">Ben Harris (bjh21)</a>.<br />Programma door Kinoko House &copy; 2013-2022.<br /><a onclick=\"javascript:openWebPage('https://kinokohouse@bitbucket.org/kinokohouse/nl-teletekst-dashboard-widget.git');\">Broncode beschikbaar onder een MIT-licentie</a>.";
    sloganCounter++;
    if (sloganCounter == sloganCarousel.length) sloganCounter = 0;
    var front = document.getElementById("front");
    var back = document.getElementById("back");

    if (window.widget) {
        widget.prepareForTransition("ToBack");
    }

    front.style.display = "none";
    back.style.display = "block";

    if (window.widget) {
        setTimeout('widget.performTransition();', 0);
    }
}

function showFront(event) {
    backOn = false;
    attachListeners();
    var front = document.getElementById("front");
    var back = document.getElementById("back");

    if (window.widget) {
        widget.prepareForTransition("ToFront");
    }

    front.style.display="block";
    back.style.display="none";

    if (window.widget) {
        setTimeout('widget.performTransition();', 0);
    }
    autoReloadTimerJiffies = getJiffies();
    if (autoReloadTimerJiffies != 0) {
        initReloadTimer();
    } else {
        stopReloadTimer();
    }
    justReloadCurrentPage();
}

if (window.widget) {
    widget.onremove = remove;
    widget.onhide = hide;
    widget.onshow = show;
    widget.onsync = sync;
}

function checkKeys(e) {
    if (!waitingForKeyUp) {
        waitingForKeyUp = true;
        if (!inputBlocked) {
            var f = e.keyCode;
            switch (f) {
                case 49: case 50: case 51: case 52:
                case 53: case 54: case 55: case 56: 
                    if (inputPage.length < 3) {
                        pageNumber(f);
                    }
                    break;
                case 48: case 57:
                    if (inputPage.length != 0 && inputPage.length < 3) {
                        pageNumber(f);
                    }
                    break;
                case 8:
                    if (inputPage.length != 0) {
                        inputPage = inputPage.substr(0,(inputPage.length - 1));
                        fillPageInput();
                    }
                    break;
             }
        }
    }
}

function pageNumber(char) {
    if (!animationRunning) {
        switch (currentlyFront) {
            case 2: case 3:
                objRotate(1);
                break;
            case 4:
                stopViewTimer();
                objRotate(1);
                break;
            case 0:
                objPrint(1);
                break;
        }
    }
    var g = char - 48;
    inputPage = inputPage + g.toString();
    fillPageInput();
}

function fillPageInput() {
    document.getElementById("pageinput").innerHTML = inputPage + "---".substr(inputPage.length);
    stopOneShot();
}

function objRotate(cf) {
    objOut(currentlyFront - 1);
    currentlyFront = cf;
    objIn(currentlyFront - 1);
}

function objPrint(cf) {
    currentlyFront = cf;
    objIn(currentlyFront - 1);
}

function objClear() {
    if (currentlyFront != 0)  {
        objOut(currentlyFront - 1);
        currentlyFront = 0;
    }
}

function pageCheck() {
  	var curPageString = "Huidige pagina: " + basePage + "-" + getSubForNumber(curSub);
    document.getElementById("curpagetext").innerHTML = curPageString;
    document.getElementById("pgnum").innerHTML = basePage;
}

function noPageCheck() {
    document.getElementById("curpagetext").innerHTML = "Pagina " + basePage + " niet beschikbaar";
    document.getElementById("pgnum").innerHTML = basePage;
}

function exceptionCheck() {
    document.getElementById("curpagetext").innerHTML = "Dienst is niet beschikbaar";
    document.getElementById("pgnum").innerHTML = "---";
}

function checkForKeyDown(e) {
    if (!animationRunning) {
        switch (e.keyCode) {
            case 16:
                switch (currentlyFront) {
                    case 3:
                        stopViewTimer();
                        objRotate(2);
                        break;
                    case 2:
                        objClear();
                        break;
                    case 0:
                        objPrint(2);
                        break;
                }
                break;
            case 91:
                switch (currentlyFront) {
                    case 2:
                        objRotate(3);
                        break;
                    case 3:
                        stopViewTimer();
                        objClear();
                        break;
                    case 0:
                        objPrint(3);
                        break;
                }
                break;
            case 37:
                if (basePage != "100" && !waitingForKeyUp)
                {
                    if (debug) alert("scanKeyDown");
                    if (scanOnCursorKeys) {
                        direction = false;
                        basePage = prevPage;
                        rummagePage(basePage, direction);
                    } else {
                        direction = false;
                        var r = parseInt(basePage,10) - 1;
                        basePage = r.toString()
                        rummagePage(basePage, direction);
                    }
                }
                break;
            case 39:
                if (basePage != "899" && !waitingForKeyUp) {
                		if (debug) alert("scanKeyUp");
                    if (scanOnCursorKeys) {
                        direction = true;
                        basePage = nextPage;
                        rummagePage(basePage, direction);
                      } else {
                        direction = true;
                        var r = parseInt(basePage, 10) + 1;
                        basePage = r.toString()
                        rummagePage(basePage, direction);
                    }
                    break;
                }
                break;
            case 27:
                if ((lastKnownGoodPage != "000" && lastKnownGoodPage != basePage) && !waitingForKeyUp && scan.flag == false) {
                    if (debug) { alert("Last known good page is " + lastKnownGoodPage + ", fetching..."); }
                    basePage = lastKnownGoodPage;
                    loadPage(basePage);
                }
                break;
            case 38:
                if (upSubAllowed) {
                    curSub++;
                    subPage = getSubForNumber(curSub);
                    getPage(basePage,subPage);
                }
                break;
            case 40:
                if (curSub > 1 && !waitingForKeyUp) {
                    curSub--;
                    subPage = getSubForNumber(curSub);
                    getPage(basePage,subPage);
                }
                break;
            case 72:
                if (!waitingForKeyUp) {
                    if (window.widget) {
                        homePage = widget.preferenceForKey("HomePage");
                    }
                    basePage = homePage;
                    loadPage(basePage);
                }
                break;
            case 83:
                if (!waitingForKeyUp) {
                    homePage = basePage;
                    if (window.widget) {
                        widget.setPreferenceForKey(homePage,"HomePage");
                    }
                    document.getElementById("pintext").innerHTML = "Startpagina: " + homePage;
                    startViewTimer();
                }
                break;
            case 188:
                if (memPointer > 1 && !waitingForKeyUp) {
                    memPointer--;
                    var c = memPointer - 1;
                    basePage = memArray[c].substring(0,3);
                    subPage = memArray[c].substring(4);
                    var pushedVal = skipMemAdd;
                    skipMemAdd = true;
                    getPage(basePage,subPage);
                    skipMemAdd = pushedVal;
                    if (memCheck || debug) {printMem();}
                }
                break;
            case 190:
                if (memPointer < memArray.length && !waitingForKeyUp) {
                    memPointer++;
                    var c = memPointer - 1;
                    basePage = memArray[c].substring(0,3);
                    subPage = memArray[c].substring(4);
                    var pushedVal = skipMemAdd;
                    skipMemAdd = true;
                    getPage(basePage,subPage);
                    skipMemAdd = pushedVal;
                    if (memCheck || debug) {printMem();}
                }
                break;
        }
    }
}
                        
function releaseKeys(e) {
    waitingForKeyUp = false;
    if (currentlyFront == 1) {
        startOneShot();
    }
}

function startOneShot() {
    stopOneShot();
    oneShot = setTimeout("firstObjOut();",waitInterval);
}

function stopOneShot() {
    if (oneShot != null) {
        clearTimeout(oneShot);
        oneShot = null;
    }    
}

function firstObjOut() {
    objOut(9);
    loadPage(basePage);
}

function objIn(x) {
    animationRunning = true;
    duration = generalDelay;
    interval = animSteps;
    start = 0;
    finish = animReady;
    end = finish;
    handler = function(animation, current, start, finish) {
        document.getElementById(objList[x]).style.top = (157 + end) - current + "px";
        document.getElementById(objList[x]).style.opacity = 0.0 + (current / end);
        if (current == end)  {
            animationRunning = false;
        }
    };
    new AppleAnimator(duration, interval, start, finish, handler).start();
}

function startViewTimer() {
    stopViewTimer();
    if (currentlyFront == 0) {
        objPrint(3);
    } else {
        if (currentlyFront == 3) {
            // do nothing
        } else {
            if (currentlyFront == 2)
            {
                objRotate(3);
            }
        }
    }
    currentlyFront = 4;
    viewTimer = setTimeout("viewOut()",waitInterval);
}

function stopViewTimer() {
    if (viewTimer != null) {
        clearTimeout(viewTimer);
        viewTimer = null;
    }
}

function viewOut() {
    objClear();
}

function objOut(x) {
    animationRunning = true;
    calledFromFirstObjOut = false;
    if (x == 9) {
        x = 0;
        inputBlocked = true;
        calledFromFirstObjOut = true;
        t = parseInt(inputPage, 10);
        if (inputPage != "") {
            if (t < 100) {
                document.getElementById("pageinput").innerHTML = basePage;
                inputPage = "";
            } else {
                basePage = inputPage;
                inputPage = "";
            }
        } else {
            document.getElementById("pageinput").innerHTML = basePage;
            inputPage = "";
        }
    }
    duration = generalDelay;
    interval = animSteps;
    start = 0;
    finish = animReady;
    end = finish;
    handler = function(animation, current, start, finish) {
        document.getElementById(objList[x]).style.top = 157 - current + "px";
        document.getElementById(objList[x]).style.opacity = 1.0 - (current / end);
        if (current == end) {
            animationRunning = false;
            if (calledFromFirstObjOut) {
                currentlyFront = 0;
                inputBlocked = false;
            }
            resetObj(x);
        }
    };
    new AppleAnimator(duration, interval, start, finish, handler).start();
}

function menuAnimationOut() {
    animationRunning = true;
    var x = currentlyFront - 1;
    duration = generalDelay;
    interval = animSteps;
    start = 0;
    finish = animReady;
    end = finish;
    handler = function(animation, current, start, finish) {
        document.getElementById(objList[x]).style.top = 157 - current + "px";
        document.getElementById(objList[x]).style.opacity = 1.0 - (current / end);
        document.getElementById("patience").style.opacity = (current / end);
        teleText.style.opacity = 1.0 - (current / end);
        if (current == end) {
            setTimeout(function(){document.getElementById("focuspuller").focus();}, 10);
            resetObj(x);
            currentlyFront = 0;
            animationRunning = false;
        }
    };
    new AppleAnimator(duration, interval, start, finish, handler).start();
}

function restorePages() {
    animationRunning = true;
    duration = generalDelay;
    interval = animSteps;
    start = 0;
    finish = animReady;
    end = finish;
    handler = function(animation, current, start, finish) {
        document.getElementById("patience").style.opacity = 1.0 - (current / end);
        teleText.style.opacity = (current / end);
        if (current == end) {
            animationRunning = false;
        }
    };
    new AppleAnimator(duration, interval, start, finish, handler).start();
}


function resetObj(x) {
    inputBlocked = true;
    document.getElementById(objList[x]).style.top = "-45px";
    document.getElementById(objList[x]).style.opacity = 1.0;
    inputBlocked = false;
}

function windowBlurred() {
    if (!backOn && transLostFocus) {
        document.body.style.opacity = 0.75;
    }
}

function windowFocus() {
    document.body.style.opacity = 1.0;
}

function pageLoadError() {
    if (!exceptFlag) {
        if (allowNonExistentPages) teleText.innerHTML = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>\r\n&#xF020;<span class=\"bg-blue \">&#xF020;&#xF020;&#xF03c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF034;&#xF020;<\/span>\r\n&#xF020;<span class=\"bg-blue \">&#xF020;&#xF020;&#xF075;&#xF070;&#xF070;&#xF030;&#xF020;&#xF060;&#xF070;&#xF070;&#xF070;&#xF070;&#xF070;&#xF070;&#xF070;&#xF070;&#xF070;&#xF070;&#xF030;&#xF020;&#xF060;&#xF070;&#xF070;&#xF070;&#xF070;&#xF070;&#xF070;&#xF070;&#xF070;&#xF070;&#xF070;&#xF070;&#xF020;&#xF020;&#xF070;&#xF070;&#xF070;&#xF035;&#xF020;<\/span>\r\n&#xF020;<span class=\"bg-blue \">&#xF020;&#xF020;&#xF020;&#xF020;&#xF020;&#xF035;&#xF020;&#xF06a;&#xF020;&#xF060;&#xF070;&#xF035;&#xF020;&#xF07f;&#xF07f;&#xF020;&#xF060;&#xF070;&#xF035;&#xF020;&#xF06a;&#xF020;&#xF060;&#xF070;&#xF035;&#xF020;&#xF035;&#xF020;&#xF035;&#xF020;&#xF070;&#xF07a;&#xF020;&#xF020;&#xF035;&#xF020;&#xF020;&#xF020;&#xF020;<\/span>\r\n&#xF020;<span class=\"bg-blue \">&#xF020;&#xF020;&#xF020;&#xF020;&#xF020;&#xF035;&#xF020;&#xF06a;&#xF020;&#xF02a;&#xF02f;&#xF035;&#xF020;&#xF07f;&#xF07f;&#xF020;&#xF02a;&#xF02f;&#xF035;&#xF020;&#xF06a;&#xF020;&#xF02a;&#xF02f;&#xF035;&#xF020;&#xF065;&#xF070;&#xF035;&#xF020;&#xF02f;&#xF06f;&#xF020;&#xF020;&#xF035;&#xF020;&#xF020;&#xF020;&#xF020;<\/span>\r\n&#xF020;<span class=\"bg-blue \">&#xF020;&#xF020;&#xF020;&#xF020;&#xF020;&#xF035;&#xF020;&#xF06a;&#xF020;&#xF068;&#xF07c;&#xF035;&#xF020;&#xF07f;&#xF07f;&#xF020;&#xF068;&#xF07c;&#xF035;&#xF020;&#xF06a;&#xF020;&#xF068;&#xF07c;&#xF035;&#xF020;&#xF034;&#xF020;&#xF07d;&#xF07c;&#xF020;&#xF06a;&#xF020;&#xF020;&#xF035;&#xF020;&#xF020;&#xF020;&#xF020;<\/span>\r\n&nbsp;<span class=\"bg-blue \">&nbsp;&#xF020;&#xF020;&#xF020;&#xF020;&#xF035;&#xF020;&#xF06a;&#xF020;&#xF022;&#xF023;&#xF035;&#xF020;&#xF023;&#xF06b;&#xF020;&#xF022;&#xF023;&#xF035;&#xF020;&#xF06a;&#xF020;&#xF022;&#xF023;&#xF035;&#xF020;&#xF035;&#xF020;&#xF037;&#xF023;&#xF020;&#xF06a;&#xF020;&#xF020;&#xF035;&#xF020;&#xF020;&#xF020;&#xF020;<\/span>\r\n<span>&#xF020;&#xF07c;&#xF07c;&#xF07c;&#xF07c;&#xF070;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF071;&#xF078;&#xF07c;&#xF07c;&#xF07c;<\/span>\r\n&nbsp;<span class=\"blue bg-white \">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>\r\n&nbsp;<span class=\"blue bg-white \">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>\r\n&nbsp;<span class=\"blue bg-white \">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Deze&nbsp;pagina&nbsp;is&nbsp;heden&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>\r\n&nbsp;<span class=\"blue bg-white \">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>\r\n&nbsp;<span class=\"blue bg-white \">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;niet&nbsp;beschikbaar&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>\r\n&nbsp;<span class=\"bg-white \">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>\r\n&nbsp;<span class=\"blue bg-white \">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>\r\n&nbsp;<span class=\"blue bg-white \">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>\r\n<span class=\"blue \">&#xF020;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;<\/span>\r\n&nbsp;<span class=\"bg-blue \">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>\r\n&nbsp;<span class=\"bg-blue \">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Druk&nbsp;op&nbsp;ESC&nbsp;om&nbsp;naar&nbsp;de&nbsp;laatst&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>\r\n&nbsp;<span class=\"bg-blue \">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>\r\n&nbsp;<span class=\"bg-blue \">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bekende&nbsp;goede&nbsp;pagina&nbsp;te&nbsp;gaan.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>\r\n&nbsp;<span class=\"bg-blue \">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>\r\n<span class=\"blue \">&#xF020;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;<\/span>\r\n&nbsp;<span class=\"bg-blue \">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;copyright&nbsp;N<\/span><span class=\"red bg-blue \">&nbsp;O<\/span><span class=\"bg-blue \">&nbsp;S&nbsp;&nbsp;2022&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>\r\n";
    } else {
        teleText.innerHTML = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>\r\n&#xF020;<span class=\"bg-red \">&#xF020;&#xF020;&#xF03c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF02c;&#xF034;&#xF020;<\/span>\r\n&#xF020;<span class=\"bg-red \">&#xF020;&#xF020;&#xF075;&#xF070;&#xF070;&#xF030;&#xF020;&#xF060;&#xF070;&#xF070;&#xF070;&#xF070;&#xF070;&#xF070;&#xF070;&#xF070;&#xF070;&#xF070;&#xF030;&#xF020;&#xF060;&#xF070;&#xF070;&#xF070;&#xF070;&#xF070;&#xF070;&#xF070;&#xF070;&#xF070;&#xF070;&#xF070;&#xF020;&#xF020;&#xF070;&#xF070;&#xF070;&#xF035;&#xF020;<\/span>\r\n&#xF020;<span class=\"bg-red \">&#xF020;&#xF020;&#xF020;&#xF020;&#xF020;&#xF035;&#xF020;&#xF06a;&#xF020;&#xF060;&#xF070;&#xF035;&#xF020;&#xF07f;&#xF07f;&#xF020;&#xF060;&#xF070;&#xF035;&#xF020;&#xF06a;&#xF020;&#xF060;&#xF070;&#xF035;&#xF020;&#xF035;&#xF020;&#xF035;&#xF020;&#xF070;&#xF07a;&#xF020;&#xF020;&#xF035;&#xF020;&#xF020;&#xF020;&#xF020;<\/span>\r\n&#xF020;<span class=\"bg-red \">&#xF020;&#xF020;&#xF020;&#xF020;&#xF020;&#xF035;&#xF020;&#xF06a;&#xF020;&#xF02a;&#xF02f;&#xF035;&#xF020;&#xF07f;&#xF07f;&#xF020;&#xF02a;&#xF02f;&#xF035;&#xF020;&#xF06a;&#xF020;&#xF02a;&#xF02f;&#xF035;&#xF020;&#xF065;&#xF070;&#xF035;&#xF020;&#xF02f;&#xF06f;&#xF020;&#xF020;&#xF035;&#xF020;&#xF020;&#xF020;&#xF020;<\/span>\r\n&#xF020;<span class=\"bg-red \">&#xF020;&#xF020;&#xF020;&#xF020;&#xF020;&#xF035;&#xF020;&#xF06a;&#xF020;&#xF068;&#xF07c;&#xF035;&#xF020;&#xF07f;&#xF07f;&#xF020;&#xF068;&#xF07c;&#xF035;&#xF020;&#xF06a;&#xF020;&#xF068;&#xF07c;&#xF035;&#xF020;&#xF034;&#xF020;&#xF07d;&#xF07c;&#xF020;&#xF06a;&#xF020;&#xF020;&#xF035;&#xF020;&#xF020;&#xF020;&#xF020;<\/span>\r\n&nbsp;<span class=\"bg-red \">&nbsp;&#xF020;&#xF020;&#xF020;&#xF020;&#xF035;&#xF020;&#xF06a;&#xF020;&#xF022;&#xF023;&#xF035;&#xF020;&#xF023;&#xF06b;&#xF020;&#xF022;&#xF023;&#xF035;&#xF020;&#xF06a;&#xF020;&#xF022;&#xF023;&#xF035;&#xF020;&#xF035;&#xF020;&#xF037;&#xF023;&#xF020;&#xF06a;&#xF020;&#xF020;&#xF035;&#xF020;&#xF020;&#xF020;&#xF020;<\/span>\r\n<span>&#xF020;&#xF07c;&#xF07c;&#xF07c;&#xF07c;&#xF070;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF071;&#xF078;&#xF07c;&#xF07c;&#xF07c;<\/span>\r\n&nbsp;<span class=\"red bg-white \">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>\r\n&nbsp;<span class=\"red bg-white \">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>\r\n&nbsp;<span class=\"black bg-white \">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;De&nbsp;dienst&nbsp;is&nbsp;heden&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>\r\n&nbsp;<span class=\"red bg-white \">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>\r\n&nbsp;<span class=\"black bg-white \">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;niet&nbsp;beschikbaar&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>\r\n&nbsp;<span class=\"bg-white \">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>\r\n&nbsp;<span class=\"red bg-white \">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>\r\n&nbsp;<span class=\"red bg-white \">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>\r\n<span class=\"red \">&#xF020;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;<\/span>\r\n&nbsp;<span class=\"bg-red \">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>\r\n&nbsp;<span class=\"bg-red \">&nbsp;&nbsp;Controleer&nbsp;uw&nbsp;internetverbinding&nbsp;of&nbsp;&nbsp;<\/span>\r\n&nbsp;<span class=\"bg-red \">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>\r\n&nbsp;<span class=\"bg-red \">&nbsp;&nbsp;kies&nbsp;een&nbsp;andere&nbsp;dienst&nbsp;uit&nbsp;het&nbsp;menu&nbsp;&nbsp;<\/span>\r\n&nbsp;<span class=\"bg-red \">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>\r\n<span class=\"red \">&#xF020;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;&#xF073;<\/span>\r\n&nbsp;<span class=\"bg-red \">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;copyright&nbsp;N<span class=\"blue \">&nbsp;O<\/span>&nbsp;S&nbsp;&nbsp;2022&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span><\/span>\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<\/span>\r\n";
    }
    if (debug) alert ("pageLoadError()");
}

function getURL(p,s) {
    return "https://teletekst-data.nos.nl/json/" + p + "-" + s;
}

function parseReply(m, pg) {
    var q0 = m.match(/Status\ \d{3}/i);
    if (q0 !== null) return "";
    nosList = eval("(" + m + ")");
    nextPage = nosList.nextPage;
    if (nextPage == "") nextPage = "100";
    if (debug) alert("Next page: " + nextPage);
    prevPage = nosList.prevPage;
    if (prevPage == "") prevPage = "100";
    if (debug) alert("Prev page: " + prevPage);
    upSubAllowed = false;
    downSubAllowed = false;
    if (nosList.prevSubPage !== "") downSubAllowed = true;
    if (nosList.nextSubPage !== "") upSubAllowed = true;
    var d = nosList.content;
    d = d.replace(/\\n/g,"<br>");
    d = d.replace(/href=\"\#(\d{3})\"/g,"href=\"void();\" onClick=\'NOSPageClicked($1,1);return;\'");
    d = d.replace(/href=\"\#(\d{3})\/(\d{1})\"/g,"href=\"void();\" onClick=\'NOSPageClicked($1,$2);return;\'");
    if (debug) { alert(d); }
    return d;
}

function openWebPage(s) {
    widget.openURL(s);
}

function toBitbucket(e) {
    widget.openURL('https://bitbucket.org/kinokohouse/nl-teletekst-dashboard-widget/src/master/');
}

function toWiki(e) {
    widget.openURL('https://en.wikipedia.org/wiki/Dashcode');
}

function NOSPageClicked(p, s) {
    if (debug) alert("Page " + p + "/" + s);
    basePage = "" + p;
    curSub = s;
    subPage = "0" + s;
    getPage(basePage, subPage);
}


function pageLoadFinished() {
    if (document.getElementById("patience").style.opacity == 1) {
        restorePages();
    }
    if (by.flag) {
        stopBusySign();
    }
    if (window.widget) {
        widget.setPreferenceForKey(basePage,widget.identifier + "-" + "basePage");
    }
    if (timerIsActive) {
        stopReloadTimer();
        initReloadTimer();
    }
    scan.flag = false;
    if (debug) {alert("pageLoadFinished();");}
}

function getSubForNumber(x) {
    if (x < 0) {
        x = 0;
    } else {
        if (x > 99) {
            x = 99;
        }
    }
    if (x < 10) {
        r = "0" + x;
    } else {
        r = x.toString();
    }
    return r;
}

function getNewPage(p) {
    subPage = "01";
    curSub = 1;
    maxSub = 1;
    basePage = p.toString();
    var vl = getPage(basePage,subPage);
    return vl;
}

function getPage(p, s) {
    var page = new XMLHttpRequest();
    try {
        page.open("GET", "https://teletekst-data.nos.nl/json/" + p + "-" + s, false);
        page.send(null);        
    }
    catch (e) {
        noPageCheck();
        if (debug) {
            alert("getPage(): an exception has occurred!");
            alert("getPage(): " + e.message);
        }
        exceptFlag = true;
        exceptionCheck();
        pageLoadError();
        return -1;
    }
    if (page.status === 200) {
        var reply = parseReply(page.responseText, p);
        var copy = "";
        if (reply == "" || !reply || reply == null) {
            pageLoadError();
            noPageCheck();
       			if (debug) { alert ("No reply: " + page.status) };
            exceptFlag = false;
            return -1;
        } else {
            copy = reply;
        }
        teleText.innerHTML = copy;        
        var z = "";
        pageLoadFinished();
        lastKnownGoodPage = p.toString();
        if (debug) { alert(lastKnownGoodPage); }
        if (!skipMemAdd) {
            addPageToMemory(p,s);
        }
        pageCheck();
        if (debug) {
            alert(txService + ": Found " + maxSub + " sub(s).");
            alert("Page " + basePage + "-" + subPage + " loaded succesfully.");
        }
        exceptFlag = false;
        return 0;
    } else {
        pageLoadError();
        noPageCheck();
        if (debug) { alert (page.status) };
        exceptFlag = false;
        return -1;
    }
}

function loadPage(p) {
    if (allowNonExistentPages != true) {
        var x = parseInt(p,10);
        var y = parseInt(lastKnownGoodPage,10);
        if (y <= x) {
            direction = true;
        } else {
            direction = false;
        }
        basePage = x.toString();
        rummagePage(basePage,direction);
    } else {
        getNewPage(p);
    }
}

function rummagePage(p, direction) {
		subPage = "01";
		curSub = 1;
		var retVal = getPage(p, subPage);
		if (retVal == -1 && !allowNonExistentPages) {
				if (direction == true) {
						basePage = nextPage;
						if (basePage.length > 3) {
								subPage = basePage.substr(4);
								curSub = parseInt(subPage);
								basePage = basePage.substr(0,3);
						}
				} else {
            basePage = prevPage;
            if (basePage.length > 3) {
                subPage = basePage.substr(4);
                curSub = parseInt(subPage);
                basePage = basePage.substr(0,3);
            }
        }
        getPage(basePage,subPage);
    }
}

function printMem() {
    var str = "";
    var v = " ";
    for (var i=0;i<memArray.length;i++) {
        v = (memPointer - 1) == i ? "|" : " ";
        str = str + v + memArray[i] + v;
    }
    alert(str);
}

function addPageToMemory(p,s) {
    if (memPointer == memArray.length) {
        if (memArray.length < maxMemSize) {
            memPointer = memArray.push(p + "-" + s);
        } else {
            var z = memArray.shift();
            memPointer = memArray.push(p + "-" + s);        
        }
    } else {
        memArray = memArray.slice(0,memPointer);
        memPointer = memArray.push(p + "-" + s);
    }
    if (memCheck || debug) {printMem();}
}

function setUpElements() {
    var n = document.getElementById("pages");
    var o = document.getElementById("pglabel");
    var p = document.getElementById("pgnum");
    var q = document.getElementById("blackbar");
    n.style.width = "400px";
    n.style.height = "288px";
    n.style.right = "12px";
    n.style.top = "36px";
    n.style.left = "13px";
    o.innerHTML = "NOS Teletekst";
    q.style.opacity = "1.0";
    q.style.display = "initial";
    homePage = getCurrentHome();
    memArray = [];
    memPointer = 0;
    lastKnownGoodPage = "000";
    basePage = homePage;
    subPage = "01";
    nextPage = "100";
    prevPage = "100";
    maxSub = 1;
    curSub = 1;
}

function startUp() {
    curSub = 1;
    maxSub = 1;
    basePage = homePage;
    subPage = "01";
    lastKnownGoodPage = "000";
    setUpElements();
    loadPage(basePage,subPage);
}

function searchPage(direction) {
    exceptFlag = false;
    if (scanOnCursorKeys) {
        scan.flag = true;
        by.counter = 0;
        if (direction) {
            scan.test = -1;
            scan.p = parseInt(basePage,10);
            if (scan.timer != null) {
                clearTimeout(scan.timer);
                scan.timer = null;
            }
            scan.timer = setTimeout("searchUp();",scan.t);
        } else {
            scan.test = -1;
            scan.p = parseInt(basePage,10);
            if (scan.timer != null)
            {
                clearTimeout(scan.timer);
                scan.timer = null;
            }
            scan.timer = setTimeout("searchDown();",scan.t);
        }
    } else {
        if (direction) {
            scan.p = parseInt(basePage,10);
            scan.p++;
            if (scan.p == 900) {scan.p = 100;}
            basePage = scan.p.toString();
            getNewPage(basePage);
        } else {
            scan.p = parseInt(basePage,10);
            scan.p--;
            if (scan.p == 99) {scan.p = 899;}
            basePage = scan.p.toString();
            getNewPage(basePage);
        }  
    }
}

function searchUp() {
    busySign();
    scan.p++;
    if (scan.p > 899) {
        scan.p = 100;
    }
    scan.test = getNewPage(scan.p.toString());
    if (scan.timer != null) {
        clearTimeout(scan.timer);
        scan.timer = null;
    }
    scan.timer = setTimeout("fromUpOn();",scan.t);
}

function searchDown() {
    busySign();
    scan.p--;
    if (scan.p < 100) {
        scan.p = 899;
    }
    scan.test = getNewPage(scan.p.toString());
    if (scan.timer != null) {
        clearTimeout(scan.timer);
        scan.timer = null;
    }
    scan.timer = setTimeout("fromDownOn();",scan.t);
}

function fromUpOn() {
    busySign();
    if (scan.test == -1 && !exceptFlag) {
        if (scan.timer != null) {
            clearTimeout(scan.timer);
            scan.timer = null;
        }
        scan.timer = setTimeout("searchUp();",scan.t);
    } else {
        if (scan.timer != null) {
            clearTimeout(scan.timer);
            scan.timer = null;
        }
        by.counter = 0;
        document.getElementById("busyimage").src = "Images/by/clear.png";
        scan.flag = false;
    }
}

function fromDownOn() {
    busySign();
    if (scan.test == -1 && !exceptFlag) {
        if (scan.timer != null) {
            clearTimeout(scan.timer);
            scan.timer = null;
        }
        scan.timer = setTimeout("searchDown();",scan.t);
    } else {
        if (scan.timer != null) {
            clearTimeout(scan.timer);
            scan.timer = null;
        }
        by.counter = 0;
        document.getElementById("busyimage").src = "Images/by/clear.png";
        scan.flag = false;
    }
}

function startBusySign() {
    if (by.timer != null) {
        clearInterval(by.timer);
        by.timer = null;
    }
    by.flag = true;
    by.timer = setInterval("busySign()",100);
}

function stopBusySign() {
    if (by.timer != null) {
        clearInterval(by.timer);
        by.timer = null;
    }
    by.counter = 0;
    document.getElementById("busyimage").src = "Images/by/clear.png";
    by.flag = false;
}

function busySign() {
    var s = by.counter < 10 ? "Images/by/0" + by.counter + ".png" : "Images/by/" + by.counter + ".png";
    document.getElementById("busyimage").src = s;
    by.counter++;
    if (by.counter > 11) by.counter = 0;
}

function coloFront() {
    document.getElementById("backstack").object.setCurrentViewWithTransition("colophon", trans);
}

function keyFront() {
    document.getElementById("backstack").object.setCurrentViewWithTransition("keys", trans);
}

function prefsFront() {
    document.getElementById("backstack").object.setCurrentViewWithTransition("prefs", trans);
}

function getPrefs() {
    if (window.widget) {
        if (widget.preferenceForKey("HomePage") === undefined) {
            widget.setPreferenceForKey("100","HomePage");
            homePage = "100";
        } else {
            homePage = widget.preferenceForKey("HomePage");
        }
        if (debug) { alert("Got homepages all right") };
        
        if (widget.preferenceForKey("transLostFocus") === undefined) {
            transLostFocus = true;
            widget.setPreferenceForKey(transLostFocus,"transLostFocus");
            document.getElementById("checkbox-translostfocus").checked = transLostFocus;
        } else {
            transLostFocus = widget.preferenceForKey("transLostFocus");
            document.getElementById("checkbox-translostfocus").checked = transLostFocus;
        }
        
        if (widget.preferenceForKey(widget.identifier + "-" + "autoReloadIndex") === undefined) {
            var autoReloadIndex = 0;
            document.getElementById("m-reload").object.setSelectedIndex(autoReloadIndex);
            autoReloadTimerJiffies = document.getElementById("m-reload").object.getValue();
            widget.setPreferenceForKey(autoReloadIndex,widget.identifier + "-" + "autoReloadIndex");
        } else {
            var autoReloadIndex = widget.preferenceForKey(widget.identifier + "-" + "autoReloadIndex");
            document.getElementById("m-reload").object.setSelectedIndex(autoReloadIndex);
            autoReloadTimerJiffies = document.getElementById("m-reload").object.getValue();
        }

        if (widget.preferenceForKey(widget.identifier + "-" + "allowNonExistentPages") === undefined) {
            allowNonExistentPages = false;
            scanOnCursorKeys = !allowNonExistentPages;
            document.getElementById("checkbox-nxpages").checked = !allowNonExistentPages;
            widget.setPreferenceForKey(allowNonExistentPages,widget.identifier + "-" + "allowNonExistentPages");
        } else {
            allowNonExistentPages = widget.preferenceForKey(widget.identifier + "-" + "allowNonExistentPages");
            scanOnCursorKeys = !allowNonExistentPages;
            document.getElementById("checkbox-nxpages").checked = !allowNonExistentPages;
        }
        
        if (widget.preferenceForKey(widget.identifier + "-" + "basePage") === undefined) {
            basePage = homePage;
            subPage = "01";
            curSub = 1;
            widget.setPreferenceForKey(basePage,widget.identifier + "-" + "basePage");
        } else {
            basePage = widget.preferenceForKey(widget.identifier + "-" + "basePage");
            subPage = "01";
            curSub = 1;
        }        
    }
}

function initWidget() {
    getPrefs();
}

function reloadMenuChanged(event) {
    var s1 = document.getElementById("m-reload").object.getSelectedIndex();
    autoReloadTimerJiffies = document.getElementById("m-reload").object.getValue();
    if (window.widget) {
        widget.setPreferenceForKey(s1,widget.identifier + "-" + "autoReloadIndex");
    }
}


function transLostFocusChanged(event) {
    transLostFocus = document.getElementById("checkbox-translostfocus").checked;
    if (window.widget) {
        widget.setPreferenceForKey(transLostFocus,"transLostFocus");
    }
}

function transLostFocusLabelClicked(event) {
    var s1 = document.getElementById("checkbox-translostfocus").checked
    document.getElementById("checkbox-translostfocus").checked = !s1;
    transLostFocus = !s1;
    if (window.widget) {
        widget.setPreferenceForKey(transLostFocus,"transLostFocus");
    }
}

function allowNXPagesChanged(event) {
    var s1 = document.getElementById("checkbox-nxpages").checked;
    allowNonExistentPages = !s1;
    scanOnCursorKeys = s1;
    if (window.widget) {
        widget.setPreferenceForKey(allowNonExistentPages,widget.identifier + "-" + "allowNonExistentPages");
    }
}

function allowNXPagesLabelClicked(event) {
    var s1 = document.getElementById("checkbox-nxpages").checked
    document.getElementById("checkbox-nxpages").checked = !s1;
    allowNonExistentPages = s1;
    scanOnCursorKeys = !s1;
    if (window.widget) {
        widget.setPreferenceForKey(allowNonExistentPages,widget.identifier + "-" + "allowNonExistentPages");
    }
}

function deleteTransientPrefs() {
    if (window.widget) {
        widget.setPreferenceForKey(null,widget.identifier + "-" + "autoReloadIndex");
        widget.setPreferenceForKey(null,widget.identifier + "-" + "allowNonExistentPages");
        widget.setPreferenceForKey(null,widget.identifier + "-" + "basePage");
    }
}

function attachListeners() {
    document.addEventListener("keydown",checkForKeyDown,true);
    document.addEventListener("keypress",checkKeys,true);
    document.addEventListener("keyup",releaseKeys,true);
}

function detachListeners() {
    document.removeEventListener("keydown",checkForKeyDown,true);
    document.removeEventListener("keypress",checkKeys,true);
    document.removeEventListener("keyup",releaseKeys,true);
}

function getCurrentHome() {
    return widget.preferenceForKey("homePage");
}

function reloadCurrentPage() {
    getPage(basePage,subPage);
    initReloadTimer();
    if (debug) alert("Page " + basePage + "-" + subPage + " autoReloaded");
}

function initReloadTimer() {
    stopReloadTimer();
    autoReloadTimerJiffies = getJiffies();
    autoReloadTimer = setTimeout("reloadCurrentPage()",autoReloadTimerJiffies);
    timerIsActive = true;
    if (debug) alert("Reload timer inited with " + autoReloadTimerJiffies + " jiffies");

}

function stopReloadTimer() {
    if (autoReloadTimer != null) {
        clearTimeout(autoReloadTimer);
        autoReloadTimer = null;
        if (debug) alert("Reload timer stopped");
        autoReloadTimerJiffies = 0;
        timerIsActive = false;
    }
}

function stopAllTimers() {
    stopOneShot();
    stopBusySign();
    stopViewTimer();
    stopReloadTimer();
    if (by.timer != null) {
        clearInterval(by.timer);
        by.timer = null;
    }
    if (scan.timer != null) {
        clearTimeout(scan.timer);
        scan.timer = null;
    }
}

function getJiffies() {
    return document.getElementById("m-reload").object.getValue();
}

function justReloadCurrentPage() {
    getPage(basePage,subPage);
    if (debug) alert("Page " + basePage + "-" + subPage + " justReloaded");
}
